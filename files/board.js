var ws // this is going to be the websocket

var pieces = ["none","url('kryds.svg')","url('bolle.svg')"];
var board  = []; board.length = 15;
var dialogWindow = document.getElementById("dialog");
var fieldIndexes = {"f1":0, "f2":1, "f3":2, "f4":3, "f5":4, "f6":5, "f7":6, "f8":7, "f9":8, "k1":9, "k2":10, "k3":11, "b1":12, "b2":13, "b3":14};
var state

var ei = -1
var moveFrom = -1

window.onload = function() {
    // prepare board
    for (i = 0 ; i < 9 ; i++) {
        board[i] = document.getElementById("f"+(i+1));
    }
    for (i = 9 ; i < 12 ; i++) {
        board[i] = document.getElementById("k"+(i-8));
    }
    for (i = 12 ; i < 15 ; i++) {
        board[i] = document.getElementById("b"+(i-11));
    }
    // connecet to websocket
    ws = new WebSocket("ws://"+window.location.host+"/wsPlayer");
    ws.onclose = function() {
        // some pop-up window saying "connection is lost, reload page"
        dialogWindow
    }
    ws.onmessage = function(msg) {
        state = JSON.parse(msg.data)
        drawConfiguration(state);

    }

}

function myClick(e) {
    ei = fieldIndexes[e.id]
    console.log("clicked on field:", ei, ", which has state:", state[ei])
    if ( state[ei] != 0) { // field is not empty
        // unselect previous selection (if any)
        if ( moveFrom != -1) {
            board[moveFrom].style.backgroundColor = "rgba(0,0,0,.0)"
        }
        // set new selection
        moveFrom = ei
        board[ei].style.backgroundColor = "rgba(0,0,0,.2)"
    } else if ( moveFrom != -1){ // field is empty AND there is a selected piece
        board[moveFrom].style.backgroundColor = "rgba(0,0,0,.0)"
        ws.send(JSON.stringify([moveFrom, ei]))
        moveFrom = -1
    }
}
function drawConfiguration(config) {
    for (i=0 ; i<15 ; i++) {
        board[i].style.backgroundImage = pieces[config[i]]; 
    }
}

