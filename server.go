package main

import (
	"fmt"
	"net/http"

	"golang.org/x/net/websocket"
)

type game struct {
	board   [15]int
	players map[string]chan [15]int // the channel sends information to players
}

type playerConnectionType struct {
	address     string
	channel     chan [15]int
	removeOrAdd int
}

var playerConnection chan playerConnectionType
var playerMove chan [2]int

func main() {
	// init stuff
	playerConnection = make(chan playerConnectionType)
	playerMove = make(chan [2]int)
	go startGameEngine(playerConnection, playerMove)

	// setup websocket serving
	http.Handle("/wsPlayer", websocket.Handler(wsPlayer))

	//Setup file server
	http.Handle("/", http.FileServer(http.Dir("./files")))

	// begin serving
	http.ListenAndServe(":8000", nil)
}

// wsPlayers handles the websocket communication with players by
//
// first sending information to the game engine about the arrival of a new player
// and then simply passing messages back and forth between the game engin and players
func wsPlayer(ws *websocket.Conn) {
	defer ws.Close()
	// add player to the game
	address := ws.Request().RemoteAddr
	board := make(chan [15]int)
	fmt.Println("New player:", address)
	playerConnection <- playerConnectionType{address, board, 1}
	// listen on ws and pass move-attempts on to the GameEngine
	go func() {
		for {
			var move [2]int
			if err := websocket.JSON.Receive(ws, &move); err != nil {
				fmt.Println("Player disconnected:" + address + ", due to error:\n  " + fmt.Sprint(err))
				playerConnection <- playerConnectionType{address, board, 0}
				break
			}
			playerMove <- move
		}
	}()
	// listen to the GameEngine and pass board-states on to ws
	for {
		if err := websocket.JSON.Send(ws, <-board); err != nil {
			fmt.Println("Player disconnected:" + address + ", due to error:\n  " + fmt.Sprint(err))
			playerConnection <- playerConnectionType{address, board, 0}
			break
		}
	}
}

// startGameEngine is the only function with access to the game/board/state
// It handles to types of events:
//
// 1) Add new players to the game engines player-map when new browsers log on
// -- and send the current board state to the new player
// 2) When players send move-suggestions, they are checked for validity and executed,
// -- and the new game/board/state is send to all players
func startGameEngine(nP chan playerConnectionType, pM chan [2]int) {
	var g game
	g.players = make(map[string]chan [15]int)
	// make initial arrangement of pieces
	for i := range g.board {
		if i >= 9 { // put crosses on all off-board fields
			g.board[i]++
		}
		if i >= 12 { // change half the off-board fields to noughts
			g.board[i]++
		}
	}
	// listen to user-input
	for {
		fmt.Println("Game engine is now listening for new players and/or moves")
		select {
		case newPlayer := <-nP:
			if newPlayer.removeOrAdd == 1 {
				g.players[newPlayer.address] = newPlayer.channel
				fmt.Printf("Player address: %q added to game engine\n", newPlayer.address)
				g.players[newPlayer.address] <- g.board
			} else if newPlayer.removeOrAdd == 0 {
				delete(g.players, newPlayer.address)
				fmt.Printf("Player address: %q removed from game engine\n", newPlayer.address)
			}
		case move := <-pM:
			fmt.Println("a player attempted the move:", move)
			fra := move[0]
			til := move[1]
			if g.board[fra] != 0 && g.board[til] == 0 {
				g.board[til] = g.board[fra]
				g.board[fra] = 0
				for _, player := range g.players {
					player <- g.board
				}
			}
		}
	}
}
