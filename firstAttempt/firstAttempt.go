package main

func main() {

}

const playsers [2]string = [2]string{"kryds", "bolle"}

type game struct {
	// the state of the game is the state of each field on the board
	// "empty", "kryds", or "bolle"
	board [15]string
	// communication channels
	players [2]chan move
}

type move struct {
	player    int
	from, too int
}

func restart(g *game) {
	for field := 0; field < 9; field++ {
		g.board[field] = "empty"
	}
	for field := 9; field < 12; field++ {
		g.board[field] = "kryds"
	}
	for field := 12; field < 15; field++ {
		g.board[field] = "bolle"
	}
	on := true
	for on {
		for i := range g.players {
			m := <-g.players[i]
			if g.validate(i, m) {

			}
		}
	}
}

func (g *game) validate(player int, m move) bool {
	if g.board[m.from] == players[player] && g.board[m.too] == "empty" {
		return true
	} else {
		return false
	}
}
